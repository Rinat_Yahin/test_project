//
//  MainAssembly.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//


import Foundation

typealias RequestCompletionBlock = (_ command: BasicRequest, _ response: ApiResponse) -> Void

class BasicRequest {
    
    // MARK: Properties
    private let session: SessionProtocol
    
    var url: URL {
        fatalError("Must override url")
    }
    
    // MARK: LifeCycle
    init(session: SessionProtocol = URLSession(configuration: URLSessionConfiguration.default)) {
        self.session = session
    }
    
    public func requestObject<T: Codable>(completion: @escaping (((Result<T, APIError>) -> Void))) {
        let processResponse = { (response: ApiResponse) in
            DispatchQueue.main.async {
                guard let data = response.data else { return }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let responseModel = try decoder.decode(T.self,
                                                           from: data)
                    completion(.success(responseModel))
                } catch let error {
                    completion(.failure(APIError.error(message: error.localizedDescription)))
                }
            }
        }
        
        request(responseHandler: processResponse) { (_, response) in
            if let error = response.error {
                completion(.failure(APIError.error(message: error.localizedDescription)))
            }
        }
    }
    
    private func request(responseHandler: @escaping (ApiResponse) -> Void, failure: @escaping RequestCompletionBlock) {
        session.dataTask(with: createUrlRequest()) { (data, response, error) in
            let apiResponse = ApiResponse(data: data, response: response, error: error)
            
            if self.responseHasError(response: apiResponse) {
                failure(self, apiResponse)
            }else {
                responseHandler(apiResponse)
            }
        }.resume()
        
      
    }
    
    private func createUrlRequest() -> URLRequest {
        let urlRequest = URLRequest(url: url)
        return urlRequest
    }
    
    private func responseHasError(response: ApiResponse) -> Bool {
        return response.error != nil
    }
 
}

protocol SessionProtocol {
    func dataTask(with request: URLRequest,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
    
}

extension URLSession: SessionProtocol {}
