//
//  MainAssembly.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//


import Foundation

final class ApiResponse {
    
    var data: Data?
    var error: APIError?
    
    init (data: Data?, response: URLResponse?, error: Error?) {
        
        if let error = error {
            self.error = APIError.error(message: error.localizedDescription)
            return
        }
        
        guard let httpData = data, httpData.count > 0 else {
                self.error = APIError.error(message: "response doesn't have any data")
            return
        }
        
        self.data = httpData
        
        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
            let message = String(format: "Error: %d", httpResponse.statusCode)
            self.error = APIError.error(message: message)
        }
        
        do {
            _ = try JSONSerialization.jsonObject(with: httpData, options: []) as? [String: Any]
        } catch {
            self.error = APIError.error(message: error.localizedDescription)
        }
    }
}
