//
//  MainAssembly.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Foundation

enum APIError: Error {
    case error(message: String)
}
