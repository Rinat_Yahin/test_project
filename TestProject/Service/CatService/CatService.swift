//
//  CatService.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Foundation
import Swinject

protocol CatServiceProtocol: class {
    func getCats(completion: @escaping (Result<[CatModel], APIError>) -> Void)
}

final class CatService: CatServiceProtocol {
    
    func getCats(completion: @escaping (Result<[CatModel], APIError>) -> Void) {
        CatRequests().requestObject(completion: completion)
    }
}

class CatServiceAssembly: Assembly {
    func assemble(container: Container) {
        container.register(CatServiceProtocol.self, factory: { r in
            return CatService()
        }).inObjectScope(.transient)
    }
}
