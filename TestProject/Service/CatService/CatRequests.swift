//
//  CatRequests.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Foundation

final class CatRequests: BasicRequest {
    
    override var url: URL {
        return URL(string: "https://cat-fact.herokuapp.com/facts/random?animal_type=cat&amount=50")!
    }
    
    func execute(completion: @escaping (Result<[CatModel], APIError>) -> Void) {
        requestObject(completion: completion)
    }
    
}

