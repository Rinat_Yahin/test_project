//
//  AppDelegate.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: RootCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        coordinator = RootCoordinator(window: window)
        coordinator?.start()
        window?.makeKeyAndVisible()

        return true
    }

}

