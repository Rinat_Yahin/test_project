//
//  CatsAssembly.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Swinject

final class CatsAssembly: Assembly {

    func assemble(container: Container) {
        container.register(CatsViewController.self, factory: { r in
            let service = r.resolve(CatServiceProtocol.self)!
            let viewModel = CatsViewModel(service: service)
            return CatsViewController(viewModel: viewModel)
        })
    }
    
}
