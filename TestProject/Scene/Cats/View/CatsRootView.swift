//
//  CatsRootView.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit
import SnapKit

final class CatsRootView: BaseRootView {
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .default
        searchBar.placeholder =  "Search facts about cats"
        searchBar.sizeToFit()
        searchBar.backgroundImage = UIImage()
        return searchBar
    }()
    
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.keyboardDismissMode = .onDrag
        return tableView
    }()
    
    // MARK: Initialization
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    private func setupViews() {
        backgroundColor = .white
        addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.size.equalTo(snp.size)
        }
        
        tableView.register(CatTableCell.self,
                           forCellReuseIdentifier: CatTableCell.cellReuseIdentifier)
    }
}

final class CatTableCell: UITableViewCell {
    
    private lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .gray
        label.backgroundColor = .white
        label.numberOfLines = 0
        return label
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.backgroundColor = .white
        addSubview(label)
        self.label.snp.makeConstraints { (make) in
            make.leading.equalTo(snp.leading).offset(10)
            make.top.equalTo(snp.top).offset(10)
            make.trailing.equalTo(snp.trailing).offset(-10)
            make.bottom.equalTo(snp.bottom).offset(-10)
        }
    }
    
    func configureCell(item: CatModel) {
        self.label.text = item.text
    }
}

