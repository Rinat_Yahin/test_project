//
//  CatsViewController.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CatsViewController: BaseViewController {
    
    private let viewModel: CatsViewModel
    private let disposeBag = DisposeBag()
    
    private lazy var rootView: CatsRootView = {
        let view = CatsRootView()
        view.tableView.dataSource = self
        view.tableView.delegate = self
        return view
    }()
    
    // MARK: Initialization
    init(viewModel: CatsViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getCats()
        bindViewModel()
        bindView()
    }
    
    override func loadView() {
        self.view = rootView
        self.navigationItem.titleView = rootView.searchBar
    }
    
    private func bindViewModel() {
        viewModel.cats.drive(onNext: { [weak self] _ in
            self?.rootView.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    private func bindView() {
        rootView.searchBar.rx
            .text
            .orEmpty
            .bind(to: viewModel.query)
            .disposed(by: disposeBag)
    }
}

extension CatsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CatTableCell.cellReuseIdentifier, for: indexPath) as! CatTableCell
        cell.configureCell(item: viewModel.itemAtIndexPath(indexPath))
        return cell
    }
}

extension CatsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
