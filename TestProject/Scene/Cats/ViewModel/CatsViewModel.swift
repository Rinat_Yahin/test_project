//
//  CatsViewModel.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol CatsViewModelInput {
    var query: PublishSubject <String> { get set }
    func getCats()
}

protocol CatsViewModelOutput {
    
    var cats: Driver<[CatModel]> { get }
    var error: Observable<APIError> { get }
    var numberOfItems: Int { get }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> CatModel
}

final class CatsViewModel {

    private let service: CatServiceProtocol
    private let items = BehaviorRelay<[CatModel]>(value: [])
    private let filteredItems = BehaviorRelay<[CatModel]>(value: [])
    private let errorSubject = PublishSubject<APIError>()
    private let disposeBag = DisposeBag()
    
    var query = PublishSubject<String>()
    
    init(service: CatServiceProtocol) {
        self.service = service
        bindInput()
    }
    
    private func bindInput() {
        Observable.combineLatest(query.asObservable(), items) { query, items in
            return items.filter { $0.text.hasPrefix(query) }
        }
        .bind(to: filteredItems)
        .disposed(by: disposeBag)
    }
}

extension CatsViewModel: CatsViewModelInput {
    
    func getCats() {
        service.getCats { [weak self] (result: Result<[CatModel], APIError>) in
            switch result {
            case .success(let cats):
                self?.items.accept(cats)
            case .failure(let error):
                self?.errorSubject.onError(error)
            }
        }
    }
}

extension CatsViewModel: CatsViewModelOutput {
    var cats: Driver<[CatModel]> {
        return self.filteredItems.asDriver(onErrorJustReturn: [])
    }
    
    var error: Observable<APIError> {
        return self.errorSubject.asObservable()
    }
    
    var numberOfItems: Int {
        return self.filteredItems.value.count
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> CatModel {
        return self.filteredItems.value[indexPath.row]
    }
}
