//
//  RootCoordinator.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    func start()
}

class RootCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    
    private var window: UIWindow?
    private let resolver = MainAssembler.shared.resolver
    private var rootNavigationViewController: UINavigationController!

    
    init(window: UIWindow?) {
        self.window = window
        self.window?.makeKeyAndVisible()
    }
    
    func start() {
        let viewController = resolver.resolve(CatsViewController.self)!
        rootNavigationViewController = UINavigationController(rootViewController: viewController)
        window?.rootViewController = rootNavigationViewController
    }
}
