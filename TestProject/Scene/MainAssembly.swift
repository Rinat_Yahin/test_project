//
//  MainAssembly.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//


import Swinject

final class MainAssembler {
    
    static let shared = MainAssembler()
    let container = Container()
    
    var resolver: Resolver {
        return assembler.resolver
    }
    
    var assembler: Assembler
    
    private init() {
        assembler = Assembler([
            CatServiceAssembly(),
            CatsAssembly()
        ])
    }
}
