//
//  Extentions.swift
//  TestProject
//
//  Created by Rinat on 14.04.2020.
//  Copyright © 2020 Rinat. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var cellReuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: cellReuseIdentifier, bundle: nil)
    }
}
